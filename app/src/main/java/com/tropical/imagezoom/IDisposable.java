package com.tropical.imagezoom;

public interface IDisposable {
	void dispose();
}
