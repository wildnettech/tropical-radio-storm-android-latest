package com.app.datamodel;

import java.io.Serializable;

public class DJ implements Serializable{
	private String image="";
	private String id="";
	private String title="";
	private String djDesc = "";
	private String email = "";
	private String descPath = "";
	
	
	private String birthday = "";
	private String yearActive = "";
	private String genres = "";
	private String influences = "";
	private String management = "";
	private String contact = "";
	private String website = "";
	private String bio = "";
	private String Collage = "";
	private String Concert = "";
	private String Clubs = "";
	private String Education = "";
	
	private int imageDrawable ; 
	
	
	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the djDesc
	 */
	public String getDjDesc() {
		return djDesc;
	}
	/**
	 * @param djDesc the djDesc to set
	 */
	public void setDjDesc(String djDesc) {
		this.djDesc = djDesc;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the descPath
	 */
	public String getDescPath() {
		return descPath;
	}
	/**
	 * @param descPath the descPath to set
	 */
	public void setDescPath(String descPath) {
		this.descPath = descPath;
	}
	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		return birthday;
	}
	/**
	 * @param birthday the birthday to set
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	/**
	 * @return the yearActive
	 */
	public String getYearActive() {
		return yearActive;
	}
	/**
	 * @param yearActive the yearActive to set
	 */
	public void setYearActive(String yearActive) {
		this.yearActive = yearActive;
	}
	/**
	 * @return the genres
	 */
	public String getGenres() {
		return genres;
	}
	/**
	 * @param genres the genres to set
	 */
	public void setGenres(String genres) {
		this.genres = genres;
	}
	/**
	 * @return the influences
	 */
	public String getInfluences() {
		return influences;
	}
	/**
	 * @param influences the influences to set
	 */
	public void setInfluences(String influences) {
		this.influences = influences;
	}
	/**
	 * @return the management
	 */
	public String getManagement() {
		return management;
	}
	/**
	 * @param management the management to set
	 */
	public void setManagement(String management) {
		this.management = management;
	}
	/**
	 * @return the contact
	 */
	public String getContact() {
		return contact;
	}
	/**
	 * @param contact the contact to set
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}
	/**
	 * @return the website
	 */
	public String getWebsite() {
		return website;
	}
	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}
	/**
	 * @return the bio
	 */
	public String getBio() {
		return bio;
	}
	/**
	 * @param bio the bio to set
	 */
	public void setBio(String bio) {
		this.bio = bio;
	}
	/**
	 * @return the collage
	 */
	public String getCollage() {
		return Collage;
	}
	/**
	 * @param collage the collage to set
	 */
	public void setCollage(String collage) {
		Collage = collage;
	}
	/**
	 * @return the concert
	 */
	public String getConcert() {
		return Concert;
	}
	/**
	 * @param concert the concert to set
	 */
	public void setConcert(String concert) {
		Concert = concert;
	}
	/**
	 * @return the clubs
	 */
	public String getClubs() {
		return Clubs;
	}
	/**
	 * @param clubs the clubs to set
	 */
	public void setClubs(String clubs) {
		Clubs = clubs;
	}
	/**
	 * @return the education
	 */
	public String getEducation() {
		return Education;
	}
	/**
	 * @param education the education to set
	 */
	public void setEducation(String education) {
		Education = education;
	}
	/**
	 * @return the imageDrawable
	 */
	public int getImageDrawable() {
		return imageDrawable;
	}
	/**
	 * @param imageDrawable the imageDrawable to set
	 */
	public void setImageDrawable(int imageDrawable) {
		this.imageDrawable = imageDrawable;
	}
	
	
}
