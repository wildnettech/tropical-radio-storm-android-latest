package com.example.radiostorm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.tropical.radiostorm.radio.R;

public class IntroActivity extends Activity {
	Button getStarted;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.intro_activity);
		
		getStarted = (Button)findViewById(R.id.getstarted);
		
		getStarted.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent HomeActivity = new Intent(IntroActivity.this, HomeActivity.class);
				startActivity(HomeActivity);
			}
		});
	}
}
