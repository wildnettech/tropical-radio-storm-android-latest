package com.example.radiostorm;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.tropical.radiostorm.radio.R;

import com.app.controller.MediaController;

public class ScheduleActivity extends Activity {
	
	ImageView PlayPauseBtn,refresh;
	MediaController mc = new MediaController();
	Button shareAppbtn;
	private int FIFTEEN_SECONDS = 10000;
	private Handler handler = new Handler();
	boolean internet_check = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.schedule);
		
		shareAppbtn = (Button)findViewById(R.id.shareappbtn);
		shareAppbtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intentt = new Intent(Intent.ACTION_VIEW);
				intentt.setAction(Intent.ACTION_SEND);
				intentt.setType("text/plain");
				intentt.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.tropical.radiostorm.radio");
				startActivityForResult(Intent.createChooser(intentt, ""), 0);
			}
		});
		
		PlayPauseBtn = (ImageView)findViewById(R.id.playnow);
		PlayPauseBtn.setTag(0);
		
		//PlayPauseBtn.setBackgroundResource(R.drawable.play);
		PlayPauseBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View v) {
				// TODO Auto-generated method stub
				switch ((Integer)((ImageView)v).getTag()) {
				case 0:
					((ImageView)v).setBackgroundResource(R.drawable.pause);
					internet_check=true;
					check_connection();
					PlayPauseBtn.setTag(1);
					
					Toast.makeText(ScheduleActivity.this, "Please wait...", Toast.LENGTH_SHORT).show();
					mc.mediaPlayStart(
							ScheduleActivity.this.getApplicationContext());
					break;
				case 1:
					((ImageView)v).setBackgroundResource(R.drawable.play);
					PlayPauseBtn.setTag(0);
					internet_check=false;
					mc.mediaPlayStop();
					break;
				default:
					break;
				}
			}
		});
		refresh = (ImageView) findViewById(R.id.refresh);
		refresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PlayPauseBtn.performClick();
				Toast.makeText(getApplicationContext(),"Refreshing...",Toast.LENGTH_LONG).show();
				PlayPauseBtn.setBackgroundResource(R.drawable.pause);
				PlayPauseBtn.setTag(1);
				mc.mediaPlayStart(
						ScheduleActivity.this.getApplicationContext());
			}
		});
	}

	Runnable mRunnable = new Runnable() {
		public void run() {

			isOnline(getApplicationContext());

			if(internet_check)
			handler.postDelayed(this, FIFTEEN_SECONDS);
		}
	};

	public void check_connection() {
		if(internet_check)
		handler.postDelayed(mRunnable, FIFTEEN_SECONDS);
	}


	public boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		internet_check=false;
		PlayPauseBtn.setBackgroundResource(R.drawable.play);
		PlayPauseBtn.setTag(0);
		mc.mediaPlayStop();
		AlertDialog alertDialog = new AlertDialog.Builder(ScheduleActivity.this).create();

		// Setting Dialog Title
		alertDialog.setTitle("Alert Dialog");

		// Setting Dialog Message
		alertDialog.setMessage("Please check your Internet Connection");

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// Write your code here to execute after dialog closed
			}
		});

		// Showing Alert Message
		alertDialog.show();
		return false;
	}
}
