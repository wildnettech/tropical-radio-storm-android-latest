package com.example.radiostorm;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.app.controller.MediaController;
import com.tropical.radiostorm.radio.R;

public class RadioActivity extends Activity {

	Button PlayPauseBtn;
	MediaController mc = new MediaController();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.radio_activity);

		PlayPauseBtn = (Button)findViewById(R.id.playbtn);
		PlayPauseBtn.setTag(0);
		PlayPauseBtn.setBackgroundResource(R.drawable.play);

		//to get the Media Player
		try {
			mc.getMediaPlayObject();
		} catch (Exception e) {
			// TODO: handle exception
		}

		PlayPauseBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				// TODO Auto-generated method stub
				String tag = ((Button)v).getTag().toString();
				if(tag.equalsIgnoreCase("0")){
					((Button)v).setBackgroundResource(R.drawable.pause);
					PlayPauseBtn.setTag(1);

					Toast.makeText(RadioActivity.this, "Please wait...", Toast.LENGTH_LONG).show();
					mc.mediaPlayStart(
							RadioActivity.this.getApplicationContext());
				}
				else if (tag.equalsIgnoreCase("1")) {
					((Button)v).setBackgroundResource(R.drawable.play);
					PlayPauseBtn.setTag(0);

					mc.mediaPlayStop();
				}

			}
		});
	}
}
