package com.example.radiostorm;

import java.util.ArrayList;
import java.util.HashMap;
import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.app.controller.Contants;
import com.app.datamodel.DJ;
import com.tropical.imagezoom.ImageViewTouch;
import com.tropical.radiostorm.radio.R;

public class DjDescActivity extends ActivityGroup {

	ImageView fbBtn;
	DJ dj;
	TextView name, desc, emailId;
	ImageView profile_Image;
	ImageViewTouch descWEbView;
	LocalActivityManager localActivityManager;
	ListView djDetailsList;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.dj_desc);
		setContentView(R.layout.dj_desc);
		
		djDetailsList = (ListView)findViewById(R.id.detaillist);
		
		dj = (DJ) this.getIntent().getSerializableExtra("dj");
		name = (TextView)findViewById(R.id.dj_name);
		emailId = (TextView)findViewById(R.id.mail_id);
		name.setText(dj.getTitle());
		desc = (TextView)findViewById(R.id.dj_detail);
		profile_Image = (ImageView)findViewById(R.id.dj_image);
		
		profile_Image.setBackgroundResource(dj.getImageDrawable());
		
		emailId.setText(dj.getEmail());
		fbBtn = (ImageView) findViewById(R.id.fb);
		fbBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_VIEW, Uri
						.parse(Contants.Facebook));
				startActivity(i);
			}
		});
		
		desc.setText(dj.getDjDesc());
		
		ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String,String>>();
		/*if(!dj.getBirthday().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Earthday:", dj.getBirthday());
			data.add(map);
		}*/
		
		if(!dj.getYearActive().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Year Active:", dj.getYearActive());
			data.add(map);
		}
		
		if(!dj.getGenres().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Genres:", dj.getGenres());
			data.add(map);
		}
		
		if(!dj.getInfluences().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Influences:", dj.getInfluences());
			data.add(map);
		}
		
		if(!dj.getManagement().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Management:", dj.getManagement());
			data.add(map);
		}
		
		if(!dj.getContact().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Contact:", dj.getContact());
			data.add(map);
		}
		
		if(!dj.getWebsite().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Website:", dj.getWebsite());
			data.add(map);
		}
		
		if(!dj.getBio().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Bio:", dj.getBio());
			data.add(map);
		}

		if(!dj.getCollage().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Past Venues:", dj.getCollage());
			data.add(map);
		}

		/*if(!dj.getConcert().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Concert:", dj.getConcert());
		}

		if(!dj.getClubs().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Club:", dj.getClubs());
			data.add(map);
		}

		if(!dj.getEducation().equals(""))
		{
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("Education:", dj.getEducation());
			data.add(map);
		}*/
		
		DjDetailAdaptor adaptor = new DjDetailAdaptor(getApplicationContext(), data);
		djDetailsList.setAdapter(adaptor);
	}
	
	class DjDetailAdaptor extends BaseAdapter{
		
		ArrayList<HashMap<String, String>> mData;
		Context mContext; 
		
		public DjDetailAdaptor(Context context, ArrayList<HashMap<String, String>> data) {
			// TODO Auto-generated constructor stub
			this.mContext = context;
			this.mData = data;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mData.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			LayoutInflater vi = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(R.layout.item_list, null);
			
			TextView title = (TextView)convertView.findViewById(R.id.title);
			TextView description = (TextView)convertView.findViewById(R.id.description);
			
			HashMap<String, String> map = mData.get(position);
			
			String titlString = null;
			String value = null;
			for ( String key : map.keySet() ) {
			    System.out.println( key );
			    titlString = key;
			    value = map.get(key);
			}
			
			title.setText(titlString);
			description.setText(value);
			
			return convertView;
		}
	}
}
