package com.example.radiostorm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.app.controller.Contants;
import com.tropical.radiostorm.radio.R;

public class ContactActivity extends Activity implements OnClickListener{
	ImageView fbBtn, twtrBtn, startBtn, youtubebtn;
	WebView tweetWebView;
	ScrollView parentScrollView;
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.contact_info);

		parentScrollView = (ScrollView) findViewById(R.id.parent_ScrollView);
		fbBtn = (ImageView)findViewById(R.id.facebookimage);
		twtrBtn = (ImageView)findViewById(R.id.twtterimage);
		startBtn = (ImageView)findViewById(R.id.startimage);
		youtubebtn = (ImageView)findViewById(R.id.youtubeimage);
	
		tweetWebView = (WebView)findViewById(R.id.tweetwebview);
		tweetWebView.getSettings().setDatabaseEnabled(true);
		tweetWebView.getSettings().setDomStorageEnabled(true);
		tweetWebView.getSettings().setJavaScriptEnabled(true);
		tweetWebView.loadUrl(Contants.TwitterPath);
		
		fbBtn.setOnClickListener(this);
		twtrBtn.setOnClickListener(this);
		startBtn.setOnClickListener(this);
		youtubebtn.setOnClickListener(this);
		parentScrollView.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event)
			{
				findViewById(R.id.tweetwebview).getParent().requestDisallowInterceptTouchEvent(false);
				return false;
			}
		});
		tweetWebView.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event)
			{

// Disallow the touch request for parent scroll on touch of
// child view
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent i;
		
		if(v==fbBtn)
		{i = new Intent(Intent.ACTION_VIEW, 
			       Uri.parse(Contants.Facebook));
			startActivity(i);
			
		}else if(v==twtrBtn)
		{i = new Intent(Intent.ACTION_VIEW, 
			       Uri.parse(Contants.Twitter));
			startActivity(i);
			
		}else if (v==youtubebtn) {
			i = new Intent(Intent.ACTION_VIEW, 
				       Uri.parse(Contants.YouTube));
				startActivity(i);
		}else if (v==startBtn) {
			i = new Intent(Intent.ACTION_VIEW, 
				       Uri.parse(Contants.ReverbNation));
				startActivity(i);
		}
	}
}

