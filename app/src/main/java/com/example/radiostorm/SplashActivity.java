package com.example.radiostorm;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.tropical.radiostorm.radio.R;

public class SplashActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Thread splashThread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Thread.sleep(4000);
					goToHomePage();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});
		
		splashThread.start();
	}
	
	private void goToHomePage() {
		// TODO Auto-generated method stub
		Intent IntroActivity = new Intent(this, IntroActivity.class);
		startActivity(IntroActivity);
		finish();
	}
}
