package com.example.radiostorm;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.app.datamodel.DJ;
import com.tropical.radiostorm.radio.R;

public class DJActivity extends Activity{
	
	ListView djListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dj);
		
		//djListView = (ListView)findViewById(R.id.djlistview);
		//ArrayList<DJ> data = new ArrayList<DJ>();
		//data = getDjList();
		//DjAdaptor adapter = new DjAdaptor(getApplicationContext(), data);
		//djListView.setAdapter(adapter);
	}
	
//	private ArrayList<DJ> getDjList() {
//		// TODO Auto-generated method stub
//		ArrayList<DJ> data = new ArrayList<DJ>();
//		DJ dj = new DJ();
//		dj.setId("0");
//		dj.setTitle("Dj Andju");
//		dj.setDjDesc("Tropical Storm Sound System INTL");
//		dj.setImage("");
//		dj.setEmail("djandju@tropicalstormsound.com");
//		dj.setDescPath("file:///android_asset/DJ_Andju_Resume-Bio.pdf");
//		dj.setBirthday("14 Nov 1981");
//		dj.setYearActive("2004 untill present");
//		dj.setGenres("Mainly Reggae/Dancehall With Hip Hop/ Top 40/ R&B/ House blends and more.");
//		dj.setInfluences("Buju Banton, Bounty Killa, Beenie Man, Queen Ifrica, Lady G, Lauren Hill, 2Pac, BIG, Beres Hammond, Supercat, Spragga, Richie Spice, Tarrus Riley, Sanchez, Garnett Silk, Freddie McGregor, Dennis Brown, Tony Matter horn, Stone Love, Bass Odyssey, Rennaisance Soundsystem, Itation Soundsystem & More.");
//		dj.setManagement("Tropical Storm Soundsystem INTL");
//		dj.setContact("djandju@tropicalstormsound.com"); //(01) (773) 972 \" 7081,
//		dj.setWebsite("http://www.tropicalstormsound.com");
//		dj.setBio("DJ Andju has emerged as a Selector DJ and has performed at numerous events including popular clubs also spinning House Music, Hip-Hop and Top 40 skillfully. DJ Andju began his musical journey as a toddler with his bass guitarist father Tony Lewis from the reggae band \"In Crowd\" based in Kingston, Jamaica, and the \"Chicago Reggae All Stars\" based in Chicago, IL. Some past performances include:");
//		dj.setImageDrawable(djimages[0]);
//		String Venues = "Antioch College \" OH, USA (2004)\nHigh Point University \" NC, USA (2006)\n"
//				+"Reggae Vibes Radio Show WLUW with Slacky J \" Loyola University, IL, USA (2005)\nDancehall Queen Chicago 2009 \" Kinetic Playground, IL, USA (2009)\nEverton Blender (Conscious Culture) \" Cubby Bear, IL, USA (2011)\nLyrics & Roots Art Movement \" The Joynt Los Angeles, CA, USA (2011)\nChicago All Star Reggae Band (Conscious Culture) \" The Metro, IL USA (2011)\nIndika/Mighty Diamonds (Conscious Culture) \" The Shrine Chicago, IL, USA (2011)\nwww.Reggae4us.com RADIO Show, Saturdays 2-4pm CDT \"Radio Broadcast with G-Sharp (2012)\n"
//				+ "The Original Lava Lounge (859 N Damen, Chicago) - IL, USA (2004)\nThe Wild Hare (alongside DJ Rufus)\" IL, USA (2004)\nJunBar/Shambles Chicago (Weekly Tuesday Reggae) \" IL, USA (2007 \" June-2012)\nExedus II (alongside One Blood Sound) \" IL, USA (2007)\nThe Darkroom (Guest Select with Slacky J) \" IL, USA (2007)\nMr Brown\"s Lounge (Weekly Saturday Reggae Nights) \" IL, USA\nThe Empire Club \" CA, USA (2011)\nUnderground Wonder Bar Chicago (Weekly Tuesday Reggae) \"IL, USA (July 2012 \" On Going)\n"
//				+"4th Annual Peace Barbeque (Association House of Chicago, Out of School Time Program) \" IL, USA (2011)\nYMCA Community Event for youth \" IL, USA (2011)\n";
//
//		dj.setCollage(Venues);
//
//		DJ dj1 = new DJ();
//		dj1.setId("0");
//		dj1.setTitle("Dj Hurricane Gilbert");
//		dj1.setDjDesc("Tropical Storm Sound System INTL");
//		dj1.setImage("");
//		dj1.setEmail("djhurricanegilbert@tropicalstormsound.com");
//		dj1.setDescPath("");
//		dj1.setImageDrawable(djimages[1]);
//
//		/*DJ dj2 = new DJ();
//		dj2.setId("0");
//		dj2.setTitle("Dj Roy");
//		dj2.setDjDesc("Tropical Storm Sound System INTL");
//		dj2.setImage("");
//		dj2.setEmail("djroy@tropicalstormsound.com");
//		dj2.setDescPath("");
//		dj2.setImageDrawable(djimages[2]);
//
//		DJ dj3 = new DJ();
//		dj3.setId("0");
//		dj3.setTitle("Zj Zabb");
//		dj3.setDjDesc("Tropical Storm Sound System INTL");
//		dj3.setImage("");
//		dj3.setEmail("zjzabb@tropicalstormsound.com");
//		dj3.setDescPath("");
//		dj3.setImageDrawable(djimages[3]);*/
//
//		data.add(dj);
//		data.add(dj1);
//		//data.add(dj2);
//		//data.add(dj3);
//
//		return data;
//	}

	int[] djimages = new int[]{R.drawable.djandju,R.drawable.dj_list_img, R.drawable.dj_list_img1,R.drawable.dj_list_img3}; 

	class DjAdaptor extends BaseAdapter{

		ArrayList<DJ> mdata = new ArrayList<DJ>();
		Context mContext;
		
		public DjAdaptor(Context context, ArrayList<DJ> data) {
			// TODO Auto-generated constructor stub
			mdata = data;
			mContext = context;
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mdata.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				LayoutInflater vi = (LayoutInflater)mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
				convertView = vi.inflate(R.layout.item_dj, null);
			}
			
			DJ dj = mdata.get(position);
			
			TextView title = (TextView)convertView.findViewById(R.id.title);
			title.setText(dj.getTitle());
			
			ImageView djPic = (ImageView)convertView.findViewById(R.id.imgview);
			djPic.setBackgroundResource(djimages[position]);
			
			convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent djDescActivity = new Intent(DJActivity.this, DjDescActivity.class);
					DJ dj = mdata.get(position);
					djDescActivity.putExtra("dj", dj);
					DJActivity.this.startActivity(djDescActivity);
				}
			});
			
			return convertView;
		}
	}
}
